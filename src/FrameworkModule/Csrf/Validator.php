<?php

namespace App\FrameworkModule\Csrf;

use Ramsey\Uuid\UuidFactoryInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class Validator
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var UuidFactoryInterface
     */
    private $uuidFactory;

    public function __construct(Session $session, UuidFactoryInterface $uuidFactory)
    {
        $this->session = $session;
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * @return string
     */
    public function getCsrfName()
    {
        return '_csrf';
    }

    /**
     * @return string
     */
    public function generateToken()
    {
        $session = &$this->session;
        $csrfName = $this->getCsrfName();

        if ($session->has($csrfName)) {
            return $session->get($csrfName);
        }

        $token = $this->uuidFactory->uuid4()->toString();
        $session->set($csrfName, $token);

        return $token;
    }

    /**
     * @param string $token
     * @return bool
     */
    public function isValid($token)
    {
        return hash_equals($this->generateToken(), $token);
    }
}
