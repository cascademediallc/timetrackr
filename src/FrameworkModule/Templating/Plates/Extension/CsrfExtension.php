<?php

namespace App\FrameworkModule\Templating\Plates\Extension;

use App\FrameworkModule\Csrf\Validator;
use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

class CsrfExtension implements ExtensionInterface
{
    /**
     * @var Validator
     */
    private $csrfValidator;

    public function __construct(Validator $csrfValidator)
    {
        $this->csrfValidator = $csrfValidator;
    }

    public function register(Engine $engine)
    {
        $engine->registerFunction('csrf', [$this, 'csrf']);
    }

    /**
     * @return string
     */
    public function csrf()
    {
        $csrfValidator = $this->csrfValidator;

        return sprintf(
            '<input type="hidden" name="%s" value="%s">',
            htmlspecialchars($csrfValidator->getCsrfName(), ENT_QUOTES | ENT_HTML5, 'UTF-8'),
            htmlspecialchars($csrfValidator->generateToken(), ENT_QUOTES | ENT_HTML5, 'UTF-8')
        );
    }
}
