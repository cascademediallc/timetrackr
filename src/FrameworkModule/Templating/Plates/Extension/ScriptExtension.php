<?php

namespace App\FrameworkModule\Templating\Plates\Extension;

use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

class ScriptExtension implements ExtensionInterface
{
    /**
     * @var array
     */
    private $scripts = [];

    public function register(Engine $engine)
    {
        $engine->registerFunction('addScripts', [$this, 'addScripts']);
        $engine->registerFunction('getScripts', [$this, 'getScripts']);
    }

    /**
     * @return array
     */
    public function getScripts()
    {
        return $this->scripts;
    }

    /**
     * @param string|array $scripts
     */
    public function addScripts($scripts = '')
    {
        $savedScripts = &$this->scripts;

        $scripts = is_array($scripts) ? $scripts : explode(' ', $scripts);

        if (!empty(array_filter($scripts))) {
            $savedScripts = array_merge($savedScripts, $scripts);
        }
    }
}
