<?php

namespace App\FrameworkModule\Templating\Plates\Extension;

use App\FrameworkModule\Routing\UrlGenerator;
use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

class UrlGeneratorExtension implements ExtensionInterface
{
    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    public function __construct(UrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function register(Engine $engine)
    {
        $engine->registerFunction('url', [$this->urlGenerator, 'url']);
        $engine->registerFunction('absoluteUrl', [$this->urlGenerator, 'absoluteUrl']);
    }
}
