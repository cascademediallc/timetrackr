<?php

namespace App\FrameworkModule\Templating\Plates\Extension;

use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

class RouteExtension implements ExtensionInterface
{
    /**
     * @var string
     */
    private $route = '';

    public function register(Engine $engine)
    {
        $engine
            ->registerFunction('getRoute', [$this, 'getRoute'])
        ;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }
}
