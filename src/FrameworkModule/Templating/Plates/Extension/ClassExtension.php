<?php

namespace App\FrameworkModule\Templating\Plates\Extension;

use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

class ClassExtension implements ExtensionInterface
{
    /**
     * @var array
     */
    private $data = [];

    public function register(Engine $engine)
    {
        $engine->registerFunction('addClass', [$this, 'addClass']);
        $engine->registerFunction('getClass', [$this, 'getClass']);
    }

    /**
     * @param string $name
     * @return string
     */
    public function getClass($name)
    {
        $data = $this->data;

        return isset($data[$name]) ? implode(' ', $data[$name]) : '';
    }

    /**
     * @param string $name
     * @param string|array $classes
     */
    public function addClass($name, $classes = '')
    {
        $savedClasses = &$this->data;

        $classes = is_array($classes) ? $classes : explode(' ', $classes);

        if (!empty(array_filter($classes))) {
            $savedClasses = array_merge_recursive($savedClasses, [$name => $classes]);
        }
    }
}
