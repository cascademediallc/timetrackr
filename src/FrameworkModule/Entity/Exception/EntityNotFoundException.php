<?php

namespace App\FrameworkModule\Entity\Exception;

use App\FrameworkModule\Entity\Exception;

class EntityNotFoundException extends \DomainException implements Exception
{

}
