<?php

namespace App\FrameworkModule;

use App\FrameworkModule\Doctrine\Dbal\CommonExpressionVisitor;
use App\FrameworkModule\Doctrine\Dbal\CriteriaExecutor;
use App\FrameworkModule\Hashing\Password\PasswordHasher;
use App\FrameworkModule\Routing\Middleware\CsrfMiddleware;
use App\FrameworkModule\Routing\Middleware\TemplatingMiddleware;
use App\FrameworkModule\Routing\UrlGenerator;
use App\FrameworkModule\Templating\Plates\Extension\ClassExtension;
use App\FrameworkModule\Templating\Plates\Extension\CsrfExtension;
use App\FrameworkModule\Templating\Plates\Extension\FormExtension;
use App\FrameworkModule\Templating\Plates\Extension\RouteExtension;
use App\FrameworkModule\Templating\Plates\Extension\ScriptExtension;
use App\FrameworkModule\Templating\Plates\Extension\UrlGeneratorExtension;
use DCP\Form\Validation\Validator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOMySql\Driver;
use League\Plates\Engine;
use Noodlehaus\Config;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Ramsey\Uuid\UuidFactory;
use Slim\App;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

class FrameworkServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $this->registerConfigService($pimple);
        $this->registerDatabaseServices($pimple);
        $this->registerSessionServices($pimple);
        $this->registerTemplatingServices($pimple);
        $this->registerRoutingServices($pimple);
        $this->registerMailerServices($pimple);
        $this->registerMiscServices($pimple);

        $settings = $pimple['settings'];

        $settings['determineRouteBeforeAppMiddleware'] = true;

        if ($pimple['debug']) {
            $settings['displayErrorDetails'] = true;
        }
    }

    private function registerConfigService(Container $pimple)
    {
        $pimple['config.path'] = __DIR__ . '/../../app/config.php';

        $pimple['config'] = function ($c) {
            return new Config($c['config.path']);
        };

        $pimple['debug'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('debug', false);
        };
    }

    private function registerDatabaseServices(Container $pimple)
    {
        $pimple['database.dsn'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('database.dsn', 'mysql:host=127.0.0.1;dbname=dev;charset=utf8');
        };

        $pimple['database.username'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('database.username', 'root');
        };

        $pimple['database.password'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('database.password', 'root');
        };

        $pimple['database.options'] = [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        ];

        $pimple['database'] = function ($c) {
            return new \PDO(
                $c['database.dsn'],
                $c['database.username'],
                $c['database.password'],
                $c['database.options']
            );
        };

        $pimple['dbal.driver'] = function () {
            return new Driver();
        };

        $pimple['dbal'] = function ($c) {
            return new Connection(
                [
                    'pdo' => $c['database']
                ],
                $c['dbal.driver']
            );
        };

        $pimple['dbal.expression_visitor'] = function () {
            return new CommonExpressionVisitor();
        };

        $pimple['dbal.criteria_executor'] = function ($c) {
            return new CriteriaExecutor($c['dbal.expression_visitor']);
        };
    }

    private function registerSessionServices(Container $pimple)
    {
        $pimple['session.storage'] = function () {
            return new NativeSessionStorage();
        };

        $pimple['session'] = function ($c) {
            return new Session($c['session.storage']);
        };
    }

    private function registerTemplatingServices(Container $pimple)
    {
        $pimple['templating.directory'] = __DIR__ . '/../../app/templates';

        $pimple['templating'] = function ($c) {
            $engine = new Engine($c['templating.directory']);
            $engine->loadExtensions([
                $c['templating.extension.class'],
                $c['templating.extension.csrf'],
                $c['templating.extension.form'],
                $c['templating.extension.route'],
                $c['templating.extension.script'],
                $c['templating.extension.url_generator']
            ]);

            return $engine;
        };

        $pimple['templating.extension.class'] = function () {
            return new ClassExtension();
        };

        $pimple['templating.extension.csrf'] = function ($c) {
            return new CsrfExtension($c['csrf.validator']);
        };

        $pimple['templating.extension.form'] = function () {
            return new FormExtension();
        };

        $pimple['templating.extension.route'] = function () {
            return new RouteExtension();
        };

        $pimple['templating.extension.script'] = function () {
            return new ScriptExtension();
        };

        $pimple['templating.extension.url_generator'] = function ($c) {
            return new UrlGeneratorExtension($c['routing.url_generator']);
        };
    }

    private function registerMailerServices(Container $pimple)
    {
        $pimple['mailer.host'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('mailer.host', 'localhost');
        };

        $pimple['mailer.port'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('mailer.port', 25);
        };

        $pimple['mailer.encryption'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('mailer.encryption', null);
        };

        $pimple['mailer.username'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('mailer.username', null);
        };

        $pimple['mailer.password'] = function ($c) {
            /** @var Config $config */
            $config = $c['config'];
            return $config->get('mailer.password', null);
        };

        $pimple['mailer.transport'] = function ($c) {
            $transport = new \Swift_SmtpTransport(
                $c['mailer.host'],
                $c['mailer.port'],
                $c['mailer.encryption']
            );

            $username = $c['mailer.username'];
            if ($username) {
                $transport->setUsername($username);
            }

            $password = $c['mailer.password'];
            if ($password) {
                $transport->setPassword($password);
            }

            return $transport;
        };

        $pimple['mailer'] = function ($c) {
            return new \Swift_Mailer($c['mailer.transport']);
        };
    }

    private function registerMiscServices(Container $pimple)
    {
        $pimple['uuid.factory'] = function () {
            return new UuidFactory();
        };

        $pimple['csrf.validator'] = function ($c) {
            return new Csrf\Validator($c['session'], $c['uuid.factory']);
        };

        $pimple['validator'] = $pimple->factory(function () {
            return new Validator();
        });

        $pimple['hasher.password'] = function () {
            return new PasswordHasher();
        };
    }

    private function registerRoutingServices(Container $pimple)
    {
        $pimple['routing.url_generator'] = function ($c) {
            return new UrlGenerator($c['router']);
        };

        $pimple['routing.middlewares.list'] = function ($c) {
            return new ArrayCollection([
                $c['routing.middleware.csrf'],
                $c['routing.middlewares.templating']
            ]);
        };

        $pimple['routing.middlewares.builder'] = $pimple->protect(function (App $app) use ($pimple) {
            foreach ($pimple['routing.middlewares.list'] as $middleware) {
                $app->add($middleware);
            }
        });

        $pimple['routing.middleware.csrf'] = function ($c) {
            return new CsrfMiddleware($c['csrf.validator']);
        };

        $pimple['routing.middlewares.templating'] = function ($c) {
            return new TemplatingMiddleware($c['templating'], $c['templating.extension.route']);
        };

        $pimple['routing.routes.list'] = function () {
            return new ArrayCollection();
        };

        $pimple['routing.routes.builder'] = $pimple->protect(function (App $app) use ($pimple) {
            foreach ($pimple['routing.routes.list'] as $routeBuilder) {
                call_user_func($routeBuilder, $app);
            }
        });
    }
}
