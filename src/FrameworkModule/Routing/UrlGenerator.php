<?php

namespace App\FrameworkModule\Routing;

use Slim\Interfaces\RouterInterface;

class UrlGenerator
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param string $name
     * @param array $data
     * @param array $queryParams
     * @return string
     */
    public function url($name, array $data = [], array $queryParams = [])
    {
        return $this->router->relativePathFor($name, $data, $queryParams);
    }

    /**
     * @param string $name
     * @param array $data
     * @param array $queryParams
     * @return string
     */
    public function absoluteUrl($name, array $data = [], array $queryParams = [])
    {
        return $this->router->pathFor($name, $data, $queryParams);
    }
}
