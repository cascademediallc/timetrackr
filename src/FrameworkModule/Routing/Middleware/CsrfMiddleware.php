<?php

namespace App\FrameworkModule\Routing\Middleware;

use App\FrameworkModule\Csrf\Validator;
use Slim\Http\Request;
use Slim\Http\Response;

class CsrfMiddleware
{
    /**
     * @var Validator
     */
    private $csrfValidator;

    public function __construct(Validator $csrfValidator)
    {
        $this->csrfValidator = $csrfValidator;
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $csrfValidator = &$this->csrfValidator;

        if ($request->isPost()) {
            $token = $request->getParam($csrfValidator->getCsrfName(), '');

            if (!$csrfValidator->isValid($token)) {
                $response
                    ->withStatus(400)
                    ->getBody()
                    ->write('Invalid CSRF'); // TODO: Make this error look better.
                return $response;
            }
        }

        return call_user_func($next, $request, $response);
    }
}
