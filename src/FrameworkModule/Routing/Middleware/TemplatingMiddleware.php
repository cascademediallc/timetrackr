<?php

namespace App\FrameworkModule\Routing\Middleware;

use App\FrameworkModule\Templating\Plates\Extension\RouteExtension;
use League\Plates\Engine;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Interfaces\RouteInterface;

class TemplatingMiddleware
{
    /**
     * @var Engine
     */
    private $templating;

    /**
     * @var RouteExtension
     */
    private $routeExtension;

    public function __construct(Engine $templating, RouteExtension $routeExtension)
    {
        $this->templating = $templating;
        $this->routeExtension = $routeExtension;
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        $this->templating->addData([
            'requestData' => $request->getParsedBody()
        ]);

        /** @var RouteInterface $route */
        $route = $request->getAttribute('route');

        if ($route) {
            $this->routeExtension->setRoute($route->getName());
        }

        return call_user_func($next, $request, $response);
    }
}
