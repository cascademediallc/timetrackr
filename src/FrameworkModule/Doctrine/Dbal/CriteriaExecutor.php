<?php

namespace App\FrameworkModule\Doctrine\Dbal;

use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\DBAL\Query\QueryBuilder;

class CriteriaExecutor
{
    /**
     * @var ExpressionVisitor
     */
    private $expressionVisitor;

    public function __construct(ExpressionVisitor $expressionVisitor)
    {
        $this->expressionVisitor = $expressionVisitor;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param Criteria $criteria
     * @return Statement|int
     */
    public function execute(QueryBuilder $queryBuilder, Criteria $criteria)
    {
        $queryBuilder
            ->where(
                $this
                    ->expressionVisitor
                    ->dispatch(
                        $queryBuilder,
                        $criteria->getWhereExpression()
                    )
            )
        ;

        if ($criteria->getFirstResult()) {
            $queryBuilder->setFirstResult($criteria->getFirstResult());
        }

        if ($criteria->getMaxResults()) {
            $queryBuilder->setMaxResults($criteria->getMaxResults());
        }

        if ($criteria->getOrderings()) {
            foreach ($criteria->getOrderings() as $field => $order) {
                $queryBuilder->addOrderBy($field, $order);
            }
        }

        return $queryBuilder->execute();
    }
}
