<?php

namespace App\FrameworkModule\Doctrine\Dbal;

use Doctrine\Common\Collections\Expr\Expression;
use Doctrine\DBAL\Query\QueryBuilder;

interface ExpressionVisitor
{
    /**
     * Dispatches walking an expression to the appropriate handler.
     *
     * @param QueryBuilder $queryBuilder
     * @param Expression $expr
     * @return mixed
     * @throws \RuntimeException
     */
    public function dispatch(QueryBuilder $queryBuilder, Expression $expr);
}
