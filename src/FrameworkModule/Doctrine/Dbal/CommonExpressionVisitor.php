<?php

namespace App\FrameworkModule\Doctrine\Dbal;

use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Doctrine\Common\Collections\Expr\Expression;
use Doctrine\Common\Collections\Expr\Value;
use Doctrine\DBAL\Query\Expression\CompositeExpression as DbalCompositeExpression;
use Doctrine\DBAL\Query\QueryBuilder;

class CommonExpressionVisitor implements ExpressionVisitor
{
    public function dispatch(QueryBuilder $queryBuilder, Expression $expr)
    {
        $result = null;

        switch (true) {
            case ($expr instanceof Comparison):
                $result = $this->walkComparison($queryBuilder, $expr);
                break;

            case ($expr instanceof Value):
                $result = $this->walkValue($queryBuilder, $expr);
                break;

            case ($expr instanceof CompositeExpression):
                $result = $this->walkCompositeExpression($queryBuilder, $expr);
                break;

            default:
                throw new \RuntimeException("Unknown Expression " . get_class($expr));
        }

        return $result;
    }

    private function walkComparison(QueryBuilder $queryBuilder, Comparison $comparison)
    {
        $operator = $comparison->getOperator();
        if ($operator === Comparison::NIN) {
            $operator = 'NOT IN';
        }

        return $queryBuilder
            ->expr()
            ->comparison(
                $comparison->getField(),
                $operator,
                $this->dispatch($queryBuilder, $comparison->getValue())
            );
    }

    private function walkValue(QueryBuilder $queryBuilder, Value $value)
    {
        $oldValue = $value->getValue();

        if (is_array($oldValue)) {
            $newValues = [];

            foreach ($oldValue as $val) {
                $newValues[] = $queryBuilder->createNamedParameter($val);
            }

            return '(' . implode(', ', $newValues) . ')';
        }

        return $queryBuilder->createNamedParameter($oldValue);
    }

    private function walkCompositeExpression(QueryBuilder $queryBuilder, CompositeExpression $expr)
    {
        $expressions = [];
        foreach ($expr->getExpressionList() as $expression) {
            $expressions[] = $this->dispatch($queryBuilder, $expression);
        }

        return new DbalCompositeExpression($expr->getType(), $expressions);
    }
}
