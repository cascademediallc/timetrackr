<?php

namespace App\FrameworkModule\Hashing;

interface HasherInterface
{
    /**
     * @param string $string
     * @return string
     */
    public function hash($string);

    /**
     * @param string $string
     * @param string $hash
     * @return bool
     */
    public function matches($string, $hash);
}
