<?php

namespace App\FrameworkModule\Hashing\Password;

use App\FrameworkModule\Hashing\HasherInterface;

class PasswordHasher implements HasherInterface
{
    public function hash($string)
    {
        return password_hash($string, PASSWORD_DEFAULT);
    }

    public function matches($string, $hash)
    {
        return password_verify($string, $hash);
    }
}
