<?php

namespace App\UserModule\Mailer;

use League\Plates\Engine;

class RegisterThanksMailer
{
    /**
     * @var Engine
     */
    private $templating;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(Engine $templating, \Swift_Mailer $mailer)
    {
        $this->templating = $templating;
        $this->mailer = $mailer;
    }

    public function send($recipient)
    {
        $data = [];

        $message = (new \Swift_Message())
            ->setSubject('Thank you for registering!')
            ->setFrom('noreply@cascademedia.us', 'Cascade Media')
            ->setTo($recipient)
            ->setBody($this->templating->render('_email/user/register/thanks', $data), 'text/html')
        ;

        $this->mailer->send($message);
    }
}
