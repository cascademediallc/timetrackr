<?php

namespace App\UserModule;

use App\UserModule\Action\RegisterAction;
use App\UserModule\Action\RegisterThanksAction;
use App\UserModule\Mailer\RegisterThanksMailer;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Slim\App;

class UserServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $this->registerActions($pimple);
        $this->registerMailers($pimple);
        $this->registerRoutes($pimple);
    }

    private function registerActions(Container $pimple)
    {
        $pimple['user.action.register'] = function ($c) {
            return new RegisterAction(
                $c['templating'],
                $c['validator'],
                $c['hasher.password'],
                $c['app.entity.repository.company'],
                $c['app.entity.repository.user'],
                $c['routing.url_generator'],
                $c['user.mailer.register_thanks']
            );
        };

        $pimple['user.action.register_thanks'] = function ($c) {
            return new RegisterThanksAction($c['templating']);
        };
    }

    private function registerMailers(Container $pimple)
    {
        $pimple['user.mailer.register_thanks'] = function ($c) {
            return new RegisterThanksMailer($c['templating'], $c['mailer']);
        };
    }

    private function registerRoutes(Container $pimple)
    {
        $pimple['routing.routes.list'][] = function (App $app) {
            $app->group('/user', function () {
                /** @var App $this */
                $this->map(['GET', 'POST'], '/register', 'user.action.register')->setName('user_register');
                $this->get('/register/thanks', 'user.action.register_thanks')->setName('user_register_thanks');

                $this->map(['GET', 'POST'], '/login', 'user.action.login')->setName('user_login');
            });
        };
    }
}
