<?php

namespace App\UserModule\Action;

use League\Plates\Engine;
use Slim\Http\Request;
use Slim\Http\Response;

class RegisterThanksAction
{
    /**
     * @var Engine
     */
    private $templating;

    public function __construct(Engine $templating)
    {
        $this->templating = $templating;
    }

    public function __invoke(Request $request, Response $response)
    {
        $response
            ->getBody()
            ->write($this->templating->render('user/register/thanks'))
        ;
    }
}
