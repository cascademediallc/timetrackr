<?php

namespace App\UserModule\Action;

use App\AppModule\Entity\Company;
use App\AppModule\Entity\Criteria\User\HasEmail;
use App\AppModule\Entity\Mapper\UserMapper;
use App\AppModule\Entity\Repository\CompanyRepository;
use App\AppModule\Entity\Repository\UserRepository;
use App\FrameworkModule\Hashing\HasherInterface;
use App\FrameworkModule\Routing\UrlGenerator;
use App\UserModule\Mailer\RegisterThanksMailer;
use DCP\Form\Validation\Constraints;
use DCP\Form\Validation\FieldReference;
use DCP\Form\Validation\Rule;
use DCP\Form\Validation\Validator;
use League\Plates\Engine;
use Slim\Http\Request;
use Slim\Http\Response;

class RegisterAction
{
    /**
     * @var Engine
     */
    private $templating;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var HasherInterface
     */
    private $passwordHasher;

    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var RegisterThanksMailer
     */
    private $mailer;

    public function __construct(
        Engine $templating,
        Validator $validator,
        HasherInterface $passwordHasher,
        CompanyRepository $companyRepository,
        UserRepository $userRepository,
        UrlGenerator $urlGenerator,
        RegisterThanksMailer $mailer
    ) {
        $this->templating = $templating;
        $this->validator = $validator;
        $this->passwordHasher = $passwordHasher;
        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
        $this->urlGenerator = $urlGenerator;
        $this->mailer = $mailer;
    }

    public function __invoke(Request $request, Response $response)
    {
        $templating = $this->templating;

        $responseData = [
            'validationResult' => null
        ];

        if ($request->isPost()) {
            $result = $this->validateForm($request);

            $responseData['validationResult'] = $result;

            if ($result->isValid()) {
                $user = UserMapper::fromArray(
                    $request->getParsedBody(),
                    UserMapper::FLAG_EXCLUDE_ID | UserMapper::FLAG_PASSWORD_IS_PLAIN
                );

                $company = new Company();
                $this->companyRepository->save($company);

                $user
                    ->setCompany($company->getId())
                    ->setPassword($this->passwordHasher->hash($user->getPlainPassword()))
                    ->setPlainPassword(null)
                ;

                $this->userRepository->save($user);

                $this->mailer->send($user->getEmail());

                return $response->withRedirect($this->urlGenerator->url('user_register_thanks'));
            }
        }

        $response
            ->getBody()
            ->write($templating->render('user/register', $responseData))
        ;

        return $response;
    }

    private function validateForm(Request $request)
    {
        $validator = $this->validator;
        $body = $request->getParsedBody();

        $this->buildValidator($validator);
        return $validator->validate($body);
    }

    private function buildValidator(Validator $validator)
    {
        $validator
            ->addRule(
                (new Rule())
                ->setFieldName('email')
                ->setMessage('Email is required')
                ->addConstraint(Constraints::notBlank())
            )
            ->addRule(
                (new Rule())
                ->setFieldName('email')
                ->setMessage('Email is not valid')
                ->addConstraint(Constraints::formatEmail())
            )
            ->addRule(
                (new Rule())
                ->setFieldName('email')
                ->setMessage('Email is already registered')
                ->addConstraint(function ($email) {
                    $users = $this->userRepository->matching(new HasEmail($email));
                    return count($users) === 0;
                })
            )
            ->addRule(
                (new Rule())
                ->setFieldName('password')
                ->setMessage('Password is required')
                ->addConstraint(Constraints::notBlank())
            )
            ->addRule(
                (new Rule())
                ->setFieldName('password')
                ->setMessage('Passwords must match')
                ->addConstraint(Constraints::mustMatch(new FieldReference('password_repeat')))
            )
        ;
    }
}
