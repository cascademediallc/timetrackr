<?php

namespace App\AppModule;

use App\AppModule\Action\IndexAction;
use App\AppModule\Entity\Repository\Doctrine\Dbal\DbalCompanyRepository;
use App\AppModule\Entity\Repository\Doctrine\Dbal\DbalUserRepository;
use App\UserModule\UserServiceProvider;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Slim\App;

class AppServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $this->registerActions($pimple);
        $this->registerRepositories($pimple);
        $this->registerRoutes($pimple);

        $pimple->register(new UserServiceProvider());
    }

    private function registerActions(Container $pimple)
    {
        $pimple['app.action.index'] = function ($c) {
            return new IndexAction($c['templating']);
        };
    }

    private function registerRepositories(Container $pimple)
    {
        $pimple['app.entity.repository.company'] = function ($c) {
            return new DbalCompanyRepository($c['dbal'], $c['dbal.criteria_executor']);
        };

        $pimple['app.entity.repository.user'] = function ($c) {
            return new DbalUserRepository($c['dbal'], $c['dbal.criteria_executor'], $c['uuid.factory']);
        };
    }

    private function registerRoutes(Container $pimple)
    {
        $pimple['routing.routes.list'][] = function (App $app) {
            $app->get('/', 'app.action.index')->setName('app_index');
        };
    }
}
