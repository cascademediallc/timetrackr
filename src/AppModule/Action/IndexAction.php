<?php

namespace App\AppModule\Action;

use League\Plates\Engine;
use Slim\Http\Request;
use Slim\Http\Response;

class IndexAction
{
    /**
     * @var Engine
     */
    private $templating;

    public function __construct(Engine $templating)
    {
        $this->templating = $templating;
    }

    public function __invoke(Request $request, Response $response)
    {
        $response
            ->getBody()
            ->write($this->templating->render('app/index'))
        ;
    }
}
