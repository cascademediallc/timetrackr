<?php

namespace App\AppModule\Entity\Repository;

use App\AppModule\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

interface UserRepository
{
    /**
     * @param Criteria $criteria
     * @return Collection|User[]
     */
    public function matching(Criteria $criteria);

    /**
     * @param User $user
     */
    public function save(User $user);
}
