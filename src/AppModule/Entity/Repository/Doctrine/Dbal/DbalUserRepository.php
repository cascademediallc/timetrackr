<?php

namespace App\AppModule\Entity\Repository\Doctrine\Dbal;

use App\AppModule\Entity\Mapper\UserMapper;
use App\AppModule\Entity\Repository\UserRepository;
use App\AppModule\Entity\User;
use App\FrameworkModule\Doctrine\Dbal\CriteriaExecutor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Connection;
use Ramsey\Uuid\UuidFactoryInterface;

class DbalUserRepository implements UserRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var CriteriaExecutor
     */
    private $criteriaExecutor;

    /**
     * @var UuidFactoryInterface
     */
    private $uuidFactory;

    public function __construct(
        Connection $connection,
        CriteriaExecutor $criteriaExecutor,
        UuidFactoryInterface $uuidFactory
    ) {
        $this->connection = $connection;
        $this->criteriaExecutor = $criteriaExecutor;
        $this->uuidFactory = $uuidFactory;
    }

    public function matching(Criteria $criteria)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('user')
        ;

        $result = $this->criteriaExecutor->execute($queryBuilder, $criteria);

        return new ArrayCollection(
            array_map(
                function ($record) {
                    return $this->hydrate($record);
                },
                $result->fetchAll()
            )
        );
    }

    public function save(User $user)
    {
        $connection = $this->connection;

        $record = UserMapper::toArray($user, UserMapper::FLAG_EXCLUDE_ID);
        unset($record['plain_password']);

        if ($user->getId()) {
            $connection->update(
                'user',
                $record,
                [
                    'id' => $user->getId()
                ]
            );
        } else {
            $user->setId($this->uuidFactory->uuid4()->toString());
            $record['id'] = $user->getId();
            $connection->insert('user', $record);
        }
    }

    private function hydrate(array $record)
    {
        return UserMapper::fromArray($record);
    }
}
