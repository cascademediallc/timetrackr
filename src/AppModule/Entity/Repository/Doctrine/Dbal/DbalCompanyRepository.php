<?php

namespace App\AppModule\Entity\Repository\Doctrine\Dbal;

use App\AppModule\Entity\Company;
use App\AppModule\Entity\Mapper\CompanyMapper;
use App\AppModule\Entity\Repository\CompanyRepository;
use App\FrameworkModule\Doctrine\Dbal\CriteriaExecutor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Connection;

class DbalCompanyRepository implements CompanyRepository
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var CriteriaExecutor
     */
    private $criteriaExecutor;

    public function __construct(Connection $connection, CriteriaExecutor $criteriaExecutor)
    {
        $this->connection = $connection;
        $this->criteriaExecutor = $criteriaExecutor;
    }

    public function matching(Criteria $criteria)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('company')
        ;

        $result = $this->criteriaExecutor->execute($queryBuilder, $criteria);

        return new ArrayCollection(
            array_map(
                function ($record) {
                    return $this->hydrate($record);
                },
                $result->fetchAll()
            )
        );
    }

    public function save(Company $company)
    {
        $connection = $this->connection;

        $record = CompanyMapper::toArray($company, CompanyMapper::FLAG_EXCLUDE_ID);

        if ($company->getId()) {
            $connection->update(
                'company',
                $record,
                [
                    'id' => $company->getId()
                ]
            );
        } else {
            $connection->insert('company', $record);
            $company->setId((int)$connection->lastInsertId());
        }
    }

    private function hydrate($record)
    {
        return CompanyMapper::fromArray($record);
    }
}
