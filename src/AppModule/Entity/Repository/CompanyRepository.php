<?php

namespace App\AppModule\Entity\Repository;

use App\AppModule\Entity\Company;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

interface CompanyRepository
{
    /**
     * @param Criteria $criteria
     * @return Collection|Company[]
     */
    public function matching(Criteria $criteria);

    /**
     * @param Company $company
     */
    public function save(Company $company);
}
