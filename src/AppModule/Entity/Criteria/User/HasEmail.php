<?php

namespace App\AppModule\Entity\Criteria\User;

use Doctrine\Common\Collections\Criteria;

class HasEmail extends Criteria
{
    /**
     * @param string $username
     */
    public function __construct($username)
    {
        parent::__construct();

        $this->where(static::expr()->eq('email', $username));
    }
}
