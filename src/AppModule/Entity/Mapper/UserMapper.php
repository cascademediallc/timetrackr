<?php

namespace App\AppModule\Entity\Mapper;

use App\AppModule\Entity\User;

class UserMapper
{
    const FLAG_EXCLUDE_ID = 1;
    const FLAG_PASSWORD_IS_PLAIN = 2;

    /**
     * @param User $user
     * @param int $flags
     * @return array
     */
    public static function toArray(User $user, $flags = 0)
    {
        $data = [
            'id' => $user->getId(),
            'company' => $user->getCompany(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'plain_password' => $user->getPlainPassword()
        ];

        if (self::hasFlag($flags, static::FLAG_EXCLUDE_ID)) {
            unset($data['id']);
        }

        return $data;
    }

    /**
     * @param mixed $data
     * @param int $flags
     * @return User
     */
    public static function fromArray($data, $flags = 0)
    {
        $defaults = [
            'id' => null,
            'company' => null,
            'email' => null,
            'password' => null,
            'plain_password' => null
        ];

        if (self::hasFlag($flags, static::FLAG_EXCLUDE_ID)) {
            unset($data['id']);
        }

        $data = array_replace($defaults, $data);

        if (self::hasFlag($flags, static::FLAG_PASSWORD_IS_PLAIN)) {
            $data['plain_password'] = $data['password'];
            $data['password'] = null;
        }

        return (new User())
            ->setId($data['id'])
            ->setCompany((int)$data['company'])
            ->setEmail($data['email'])
            ->setPassword($data['password'])
            ->setPlainPassword($data['plain_password'])
        ;
    }

    private static function hasFlag($flags, $flag)
    {
        return ($flags & $flag) === $flag;
    }
}
