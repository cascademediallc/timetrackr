<?php

namespace App\AppModule\Entity\Mapper;

use App\AppModule\Entity\Company;

class CompanyMapper
{
    const FLAG_EXCLUDE_ID = 1;

    /**
     * @param Company $company
     * @param int $flags
     * @return array
     */
    public static function toArray(Company $company, $flags = 0)
    {
        $data = [
            'id' => $company->getId(),
            'name' => $company->getName()
        ];

        if (self::hasFlag($flags, static::FLAG_EXCLUDE_ID)) {
            unset($data['id']);
        }

        return $data;
    }

    /**
     * @param mixed $data
     * @param int $flags
     * @return Company
     */
    public static function fromArray($data, $flags = 0)
    {
        $defaults = [
            'id' => null,
            'name' => null
        ];

        if (self::hasFlag($flags, static::FLAG_EXCLUDE_ID)) {
            unset($data['id']);
        }

        $data = array_replace($defaults, $data);

        return (new Company())
            ->setId((int)$data['id'])
            ->setName($data['name'])
        ;
    }

    private static function hasFlag($flags, $flag)
    {
        return ($flags & $flag) === $flag;
    }
}
