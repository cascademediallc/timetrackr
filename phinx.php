<?php

namespace runtime;

require __DIR__ . '/vendor/autoload.php';

use Interop\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container = require __DIR__ . '/app/di.php';

return [
    'paths' => [
        'migrations' => __DIR__ . '/app/database/migrations',
        'seeds' => __DIR__ . '/app/database/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'development' => [
            'name' => 'dev',
            'connection' => $container->get('database')
        ]
    ]
];
