<footer id="main-footer" class="<?php $this->getClass('main-footer') ?>">
    &copy;<?= date('Y') ?> Cascade Media, LLC
</footer>
