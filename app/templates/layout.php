<?php
/** @var \League\Plates\Template\Template $this */
?>
<!doctype html>
<html lang="en">
<head>
    <?php $this->insert('_layout/head', $this->data) ?>
    <?php $this->insert('_layout/styles', $this->data) ?>
</head>
<body class="<?= $this->getClass('body') ?>">
    <div class="site-wrapper">
        <?php if ($this->section('main-header')) { ?>
            <header id="main-header" class="<?= $this->getClass('main-header') ?>">
                <?= $this->section('main-header') ?>
            </header>
        <?php } else { ?>
            <?php $this->insert('_headers/main-default', $this->data) ?>
        <?php } ?>

        <main id="main-content" class="<?= $this->getClass('main-content') ?>">
            <?php $this->insert('_layout/flash', $this->data) ?>

            <?php if ($this->section('page-header')) { ?>
                <header class="page-header <?= $this->getClass('page-header') ?>">
                    <?= $this->section('page-header') ?>
                </header>
            <?php } ?>

            <section class="page-content <?= $this->getClass('page-content') ?>">
                <?= $this->section('page-content') ?>
            </section>

            <?php if ($this->section('page-aside')) { ?>
                <aside class="page-aside <?= $this->getClass('page-aside') ?>">
                    <?= $this->section('page-aside') ?>
                </aside>
            <?php } ?>
        </main>

        <?php if ($this->section('main-footer')) { ?>
            <footer id="main-footer" class="<?php $this->getClass('main-footer') ?>">
                <?= $this->section('main-footer') ?>
            </footer>
        <?php } else { ?>
            <?php $this->insert('_footers/main-default', $this->data) ?>
        <?php } ?>
    </div>

    <?php $this->insert('_layout/javascript', $this->data) ?>
</body>
</html>
