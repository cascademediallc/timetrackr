<?php
/** @var \League\Plates\Template\Template $this */
$this->layout('layout');
?>

<?php $this->start('page-content') ?>
<div>
    Thank you for registering. You can now <a href="<?= $this->e($this->url('user_login')) ?>">log in</a>!
</div>
<?php $this->stop() ?>
