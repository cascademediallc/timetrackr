<?php
/** @var \League\Plates\Template\Template $this */
$this->layout('layout');
?>

<?php $this->start('page-content') ?>
<div>
    Register page from Plates!
</div>
<form action="" method="post">
    <?= $this->csrf() ?>
    Email: <input type="text" name="email" value=""><br>
    Password: <input type="password" name="password"><br>
    Repeat Password: <input type="password" name="password_repeat"><br>
    <button type="submit">Register</button>
</form>
<?php $this->stop() ?>
