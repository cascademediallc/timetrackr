<header id="main-header" class="<?= $this->getClass('main-header') ?>">
    <a class="site-brand" href="">Squidlo</a>
    <nav class="main-menu" role="navigation">
        <ul>
            <li class=""><a href="index.php">Home</a></li>
            <li class=""><a href="typography.php">Typography</a></li>
            <li class=""><a href="grid.php">Grids</a></li>
            <li class=""><a href="menus.php">Menus</a></li>
            <li class=""><a href="components.php">Components</a></li>
            <li class=""><a href="table.php">Tables</a></li>
            <li class=""><a href="forms.php">Forms</a></li>
            <li class=""><a href="layouts.php">Layouts</a></li>
        </ul>
    </nav>
</header>
