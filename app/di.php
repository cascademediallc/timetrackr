<?php

namespace runtime;

use App\AppModule\AppServiceProvider;
use App\FrameworkModule\FrameworkServiceProvider;
use Slim\Container;

$container = new Container();
$container->register(new FrameworkServiceProvider());
$container->register(new AppServiceProvider());

return $container;
