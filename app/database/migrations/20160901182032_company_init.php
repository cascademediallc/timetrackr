<?php

use Phinx\Migration\AbstractMigration;

class CompanyInit extends AbstractMigration
{
    public function up()
    {
        $this
            ->table('company')
            ->addColumn('name', 'string')
            ->save()
        ;

        $this
            ->table('user')
            ->addColumn('company', 'integer', ['after' => 'id'])
            ->addForeignKey('company', 'company', 'id')
            ->save()
        ;
    }

    public function down()
    {
        $this
            ->table('user')
            ->dropForeignKey('company')
            ->removeColumn('company')
            ->save()
        ;

        $this->dropTable('company');
    }
}
