<?php

use Phinx\Migration\AbstractMigration;

class UserInit extends AbstractMigration
{
    public function up()
    {
        $this
            ->table('user', [
                'id' => false,
                'primary_key' => 'id'
            ])
            ->addColumn('id', 'string', ['length' => 36])
            ->addColumn('email', 'string')
            ->addIndex(['email'], ['unique' => true])
            ->addColumn('password', 'string')
            ->save()
        ;
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
