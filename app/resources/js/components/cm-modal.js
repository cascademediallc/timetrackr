(function ($) {
    $(function () {

        var defaults = {
            template: [
                '<section class="ui-modal">',
                '   <section class="modal-box">',
                '       <header><h1 data-modal-title></h1></header>',
                '       <div class="modal-scroller" data-modal-scroller>',
                '           <div class="modal-content" data-modal-content></div>',
                '       </div>',
                '       <footer><div class="modal-close" data-modal-close></div></footer>',
                '   </section>',
                '</section>'
            ].join("\n"),
            selectors: {
                close: '[data-modal-close]',
                content: '[data-modal-content]',
                scroller: '[data-modal-scroller]',
                title: '[data-modal-title]'
            }
        };

        var Modal = function (options) {
            this.setOptions(options);
            this.template = hm.extend({}, this.template);
        };

        Modal.prototype = {
            initialized: false,
            modals: [],
            options: null,
            timeout: null,
            template: {
                close: null,
                container: null,
                content: null,
                scroller: null,
                title: null
            }
        };

        Modal.prototype.create = function () {
            if (this.initialized) return this;

            this.initialized = true;
            this.modals.push(this);

            var options = this.options;
            var template = $(options.template);

            if (!template.length) {
                console.log('cm.modal: Template not found - "' + options.template + '"');
                return null;
            }

            if (document.contains(template[0])) {
                var hasInnerElement = !!template[0].children.length;
                var original = template;

                template = hasInnerElement ? $(template[0].children[0]) : $(template[0].innerHTML);

                if (hasInnerElement) {
                    document.body.appendChild(template[0]);
                }

                original.remove();
            }

            var close = template.find(options.selectors.close);

            this.template.close = close;
            this.template.container = template;

            this.template.content = template.find(options.selectors.content);
            this.template.scroller = template.find(options.selectors.scroller);
            this.template.title = template.find(options.selectors.title);

            close.on('click', function () {
                this.close();
            }.bind(this));

            template.on('click', function (event) {
                var target = $(event.target);
                var parent = target.parent();

                if (!template[0].contains(parent[0])) {
                    this.close();
                }
            }.bind(this));

            if (!document.contains(template[0])) {
                document.body.appendChild(template[0]);
            }

            return this;
        };

        Modal.prototype.open = function (data) {
            if (!this.create()) return;

            hm.each(this.modals, function (item) {
                if (item != this) {
                    item.close();
                }
            }.bind(this));

            var details = hm.extend({content: null, title: null}, data);

            this.setContent(details.content);
            this.setTitle(details.title);

            this.template.container.show();
            this.template.scroller.css('height', this.template.scroller.parent().height());

            return this;
        };

        Modal.prototype.close = function () {
            this.template.container.hide();
            this.template.scroller.css('height', 'auto');

            return this;
        };

        Modal.prototype.setData = function (element, data) {
            if (!data) return;

            element.empty();

            if (data[0] instanceof HTMLElement) {
                var hasInnerElement = !!data[0].children.length;
                var template = hasInnerElement ? $(data[0].children[0])[0] : $(data[0].innerHTML)[0];

                if (!template) {
                    template = document.createTextNode(data[0].innerHTML);
                }

                element[0].appendChild(template);
            } else {
                element.html(data);
            }

            return this;
        };

        Modal.prototype.setContent = function (data) {
            this.setData(this.template.content, data);
            return this;
        };

        Modal.prototype.setOptions = function (options) {
            this.options = hm.extend({}, defaults, options);
            return this;
        };

        Modal.prototype.setTitle = function (data) {
            this.setData(this.template.title, data);
            return this;
        };

        /**
         * data-type: defines component entry point
         * data-{component}-template: defines template for component
         * data-{component}-{action}: defines specific actions for template
         * data-{component}-{section}: defines specific sections for template
         * data-{component}-{var}: defines specific variables for component
         */

        /* Register with data attributes */
        var modals = $('[data-type~="cm-modal"]');

        hm.each(modals, function (item) {
            var element = $(item);
            var data = element.data();

            var options = {};
            var title = data.modalTitle ? $(data.modalTitle) : null;
            var content = data.modalContent ? $(data.modalContent) : null;

            if (data.modalTemplate) {
                options.template = data.modalTemplate;
            }

            var modal = new Modal(options).create();
            var initialized = false;

            element.on('click', function (event) {
                if (data.modalPreventDefault) event.preventDefault();

                if (!initialized) {
                    modal.setTitle(title);
                    modal.setContent(content);
                    initialized = true;
                }

                modal.open();
            });
        });

        /* Register with Globals */
        if (typeof window !== 'undefined') {
            window = hm.extend(window, {cm: {modal: Modal}});
        }

        /* Register with AMD */
        if (typeof define !== 'undefined') {
            define('cm-modal', [], function() {
                return Modal;
            });
        }

        /* Register with CommonJS */
        if (typeof module !== 'undefined') {
            module.exports = Modal;
        }
    });
})(jQuery);
