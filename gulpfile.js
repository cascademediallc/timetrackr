
// libraries
var _ = require('lodash');
var colors = require('colors');
var concat = require('gulp-concat');
var fs = require('fs');
var gulp = require('gulp');
var notifier = require('node-notifier');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglifyjs = require('gulp-uglify');
var util = require('gulp-util');

// functions
var exec = require('child_process').exec;
var sprintf = require('sprintf-js').sprintf;

// tools
var browser = require('browser-sync').create();

// variables
var watchers = [];

// configurations
var config = {
    assets: {
        origin: 'app/resources',
        target: 'web/resources',
        glob: [
            'app/resources/fonts/**/*',
            'app/resources/images/**/*'
        ]
    },
    js: {
        libs: {
            origin: 'app/resources/js/libs',
            target: 'web/resources/js/libs.js',
            glob: [
                'jquery.min.js',
                'helpme.js'
            ]
        },
        src: {
            target: 'web/resources/js/main.js',
            origin: 'app/resources/js',
            glob: [
                'components/**.js',
                'main.js'
            ]
        }
    },
    sass: {
        outputStyle: 'compressed',
        precision: 9,
        origin: 'app/resources/sass',
        target: 'web/resources/css',
        file: {
            css: '/main.css',
            sass: '/main.scss'
        },
        glob: [
            '**/*.scss'
        ]
    },
    sync: {
        glob: [
            'web/**/*',
            'app/templates/**/*'
        ],
        server: {
            host: '127.0.0.1',
            proxy: 'http://localhost:8081/',
            port: 8082,
            notify: false
        }
    },
    production: !!util.env.production,
    sourcemaps: !!util.env.sourcemaps,
    vagrant: true
};

// helpers
var compileJS = function (options) {
    var file = options.target.split('/').pop();
    var destination = options.target.replace('/' + file, '');
    var pattern = getPattern(options);

    gulp.src(pattern)
        .pipe(config.sourcemaps ? sourcemaps.init() : util.noop())
        .pipe(config.production ? uglifyjs() : util.noop())
        .pipe(concat(file))
        .pipe(config.sourcemaps ? sourcemaps.write() : util.noop())
        .pipe(gulp.dest(destination))
        .on('finish', function () {
            console.log('js:compile', 'write'.green, options.target);
        });
};

var getPattern = function (options) {
    return _.map(options.glob, function (item) {
        return options.origin + '/' + item;
    });
};

// local tasks
gulp.task('assets:compile', function () {
    var options = config.assets;

    gulp.src(options.glob, {base: './' + options.origin})
        .pipe(gulp.dest(options.target))
        .on('finish', function () {
            var files = options.glob.join(', ')
                .replace(new RegExp(options.origin, 'g'), options.target);

            console.log('assets:compile', 'write'.green, files);
        });
});

gulp.task('assets:watch', ['assets:compile'], function () {
    var options = config.assets;
    var cwd = process.cwd();

    watchers.push(gulp.watch(options.glob, function (event) {
        var type = event.type;
        var origin = event.path.replace(cwd + '/', '');
        var file = origin.split('/').pop();
        var destination = origin.replace(options.origin, options.target);

        switch (type) {
            case 'deleted':
                fs.unlink(destination, util.noop);
                break;
            default:
                gulp.src(origin)
                    .pipe(gulp.dest(destination.replace('/' + file, '')))
        }

        console.log('assets:watch', type.green, destination);
    }));
});

gulp.task('browser:watch', function () {
    browser.init(config.sync.server);

    watchers.push(gulp.watch(config.sync.glob, browser.reload));

    if (config.vagrant) {
        exec('vagrant status', function (err, stdout, stderr) {
            if (!_.includes(stdout, 'running')) {
                console.log('browser:watch', 'error'.red, 'ensure vagrant is running with: "vagrant up"');
                console.log('browser:watch', 'error'.red, 'stopping watchers');

                _.each(watchers, function (watcher) {
                    watcher.end();
                });
            }
        });
    }
});

gulp.task('js:compile:libs', function () {
    compileJS(config.js.libs);
});

gulp.task('js:compile:src', function () {
    compileJS(config.js.src);
});

gulp.task('js:compile', ['js:compile:libs', 'js:compile:src']);

gulp.task('js:watch', ['js:compile'], function () {
    watchers.push(gulp.watch(getPattern(config.js.libs), ['js:compile:libs']));
    watchers.push(gulp.watch(getPattern(config.js.src), ['js:compile:src']));
});

gulp.task('sass:compile', function () {
    var options = config.sass;
    var destination = options.target;

    var showError = function (error) {
        console.log('sass:compile', 'error'.red, error.message);

        notifier.notify({
            title: 'SASS Watcher',
            message: 'File: "' + error.file.replace(process.cwd(), '') + '" Line: ' + error.line + '\n' + error.messageOriginal
        });
    };

    gulp.src(options.origin + options.file.sass)
        .pipe(sass(options).on('error', showError))
        .pipe(gulp.dest(destination))
        .on('finish', function () {
            console.log('sass:compile', 'write'.green, options.target + options.file.css);
        });
});

gulp.task('sass:watch', ['sass:compile'], function () {
    console.log(getPattern(config.sass));

    watchers.push(gulp.watch(getPattern(config.sass), ['sass:compile']));
});

// global tasks
gulp.task('default', ['js:watch', 'sass:watch', 'assets:watch', 'browser:watch']);
gulp.task('compile', ['js:compile', 'sass:compile', 'assets:compile']);
gulp.task('watch', ['js:watch', 'sass:watch', 'assets:watch']);
