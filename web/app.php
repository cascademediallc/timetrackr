<?php

namespace runtime;

require __DIR__ . '/../vendor/autoload.php';

use Interop\Container\ContainerInterface;
use Slim\App;

/** @var ContainerInterface $container */
$container = require __DIR__ . '/../app/di.php';

$app = new App($container);
$middlewares = $container->get('routing.middlewares.builder');
$middlewares($app);

$routeBuilder = $container->get('routing.routes.builder');
$routeBuilder($app);

$app->run();
